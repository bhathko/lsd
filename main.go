package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gocolly/colly"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"regexp"
)

type ItemsObj struct {
	ProductURL string `json:"productUrl"`
}

type Sticker struct {
	Items []ItemsObj `json:"items"`
}
var collectionId string
var collection string

func main()  {
	if len(os.Args) > 1 {
		collection = os.Args[1]
	}
	stickerCollection := searchSticker()
	for _, stickerUrl := range stickerCollection.Items {
		scrapePage(stickerUrl.ProductURL)
	}

}

func scrapePage(url string) {
	host := "https://store.line.me"
	re, _ := regexp.Compile(`\d{2,10}`)
	collectionId = re.FindString(url)
	mkDir()
	c := colly.NewCollector(
		// Visit only domains: hackerspaces.org, wiki.hackerspaces.org
		colly.AllowedDomains("store.line.me"),
	)

	// On every a element which has href attribute call callback
	c.OnHTML("span[style]", func(e *colly.HTMLElement) {
		link := e.Attr("style")
		num := len(link)
		re, _ := regexp.Compile(`\d{2,10}`)
		result := re.FindString(link)
		imagePath := link[21:(num-2)]
		err := downloadSticker(imagePath, result)
		if err != nil {
			log.Fatal(err)
		}

	})

	c.OnRequest(func(r *colly.Request) {
		fmt.Println("Visiting", r.URL.String())
	})

	c.Visit(host + url)
}

func downloadSticker(url string, fileName string) error{
	response, err := http.Get(url)
	if err != nil {
		return err
	}
	defer response.Body.Close()
	if response.StatusCode != 200 {
		return errors.New("received non 200 response code")
	}
	file, err := os.Create( collection + "/" + collectionId + "/" + fileName +".png")
	if err != nil {
		return err
	}
	defer file.Close()
	//Write the bytes to the file
	_, err = io.Copy(file, response.Body)
	if err != nil {
		return err
	}

	return nil

}

func searchSticker() Sticker{
	params := url.Values{}
	params.Add("query", collection)
	resp, err := http.Get("https://store.line.me/api/search/sticker?" + params.Encode() + "&offset=0&limit=36&type=ALL&includeFacets=true")
	if err != nil {
		log.Fatal(err)
	}
	var msg Sticker
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	err = json.Unmarshal(body, &msg)
	return msg
}

func mkDir() {
	fmt.Println(collection, collectionId)
	if _, err := os.Stat(collection + string('/') + collectionId); os.IsNotExist(err) {
		err := os.MkdirAll(collection + string('/') + collectionId, 0755)
		if err != nil {
			log.Fatal(err)
		}
	}
}